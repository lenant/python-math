# coding=utf-8
import math
import matplotlib as mpl
import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import LinearLocator, FormatStrFormatter


def hyperboloid_with_tan(x, y):
    return [[math.tan(a ** 2) for a in b] for b in x] + y ** 2


def hyperboloid(x, y):
    return x ** 2 + y ** 2


def derivative_x(f, x, y0, delta=0.0001):
    res = (f(x + delta, y0) - f(x, y0)) / delta
    return res


def derivative_y(f, x0, y, delta=0.0001):
    f1 = f(x0, y + delta)
    # print "f1=", f1
    f2 = f(x0, y)
    # print "f2=", f2
    # print "f1-f2=", f1-f2
    res = (f1 - f2) / delta
    return res


def hyperboloid_tangent(x, x0, y, y0):
    # f'x(x0, y0) (x − x0) + f'y(x0, y0) (y − y0) − (z − z0) = 0.
    return 2 * x0 * (x - x0) + 2 * y0 * (y - y0) + hyperboloid(x0, y0)


def tangent(f, x, x0, y, y0):
    # f'x(x0, y0) (x − x0) + f'y(x0, y0) (y − y0) − (z − z0) = 0.

    # print "derivative_x(f, x, y0)", derivative_x(f, x, y0)
    # print "2x", 2 * x
    # print "derivative_x(f, x, y0) * (x - x0)", derivative_x(f, x, y0) * (x - x0)
    # print "2x * (x - x0)", 2 * x * (x - x0)
    # print "derivative_y(f, x0, y)", derivative_y(f, x0, y)
    # print "2y", 2 * y
    # print "derivative_y(f, x0, y) * (y - y0)", derivative_y(f, x0, y) * (y - y0)
    # print "2y * (y - y0)", 2 * y * (y - y0)
    # print "f(x0, y0)", f(x0, y0)
    # print "hyperboloid(x0, y0)", hyperboloid(x0, y0)
    # print "derivative_x(f, x, y0) * (x - x0) + derivative_y(f, x0, y) * (y - y0)", derivative_x(f, x, y0) * (x - x0) + derivative_y(f, x0, y) * (y - y0)
    # print "2 * x0 * (x - x0) + 2 * y0 * (y - y0)", 2 * x0 * (x - x0) + 2 * y0 * (y - y0)
    return derivative_x(f, x0 * np.ones(len(x)), y0) * (x - x0) + derivative_y(f, x0, y0 * np.ones(len(y))) * (y - y0) + f(x0, y0)


# mpl.rcParams['legend.fontsize'] = 10
# fig = plt.figure()
# ax = fig.gca(projection='3d')


# --------------

def main():
    x = np.outer(np.linspace(-2, 2, 30), np.ones(30))
    y = x.copy().T

    x0 = 0.7
    y0 = 1.6
    f = hyperboloid
    # print 2 * y
    # print derivative_x(f, x, y0)
    # print derivative_y(f, 123, y)
    # print hyperboloid_tangent(x, x0, y, y0)
    # print tangent(hyperboloid, x, x0, y, y0)

    z = f(x, y)
    print 'x=', x
    print 'y=', y
    print 'z=', z

    fig = plt.figure()
    ax = plt.axes(projection='3d')

    ax.plot_surface(x, y, z, cmap=plt.cm.jet, rstride=1, cstride=1, linewidth=0)

    # z = hyperboloid_tangent(x, x0, y, y0)
    z = tangent(f, x, x0, y, y0)
    print 'x=', x
    print 'y=', y
    print 'z=', z
    ax.plot_surface(x, y, z, cmap=plt.cm.jet, rstride=1, cstride=1, linewidth=0)

    plt.show()


if __name__ == "__main__":
    main()
