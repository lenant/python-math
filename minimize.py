import math
from matplotlib import pylab as plt
import numpy as np
from scipy import optimize


def task_func(x):
    return math.sin(x / 5.) * math.exp(x / 10.) + 5. * math.exp(-x / 2.)


def int_task_func(x):
    return int(task_func(int(x)))

def iter_printer(v):
    print v

def main():
    x_array = np.arange(1, 40, 0.1)
    plt.plot(x_array, [int_task_func(a) for a in x_array])
    plt.show()

    print optimize.minimize(int_task_func, np.array([5]), callback=iter_printer, method="BFGS")
    print
    print optimize.differential_evolution(int_task_func, [(1, 30)])

    # print optimize.minimize(task_func, np.array([30]), callback=iter_printer)
    # print "%.2f" % optimize.minimize(task_func, np.array([30]), callback=iter_printer, method="BFGS")["fun"]
    # print optimize.differential_evolution(task_func, [(1, 30)])

    # for x in range(1, 30):
    #     print "x =", x
    #     print optimize.minimize(f, np.array([x]), callback=iter_printer, method="BFGS")
    #     print

if __name__ == "__main__":
    main()