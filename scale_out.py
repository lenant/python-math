import pandas as pd
import numpy as np


def scale_out(x):
    median = np.median(x)
    print median
    std = np.std(x)
    print std
    res = (x - median) / std
    print res.mean()


def main():
    array = np.array([5, 21, 8, 0, -1, 4, 6, 2, -7, 2, 2, 12, -7, 4, -9, 10000 ])
    scale_out(array)


if __name__ == "__main__":
    main()

