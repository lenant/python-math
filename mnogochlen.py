import math
from matplotlib import pylab as plt
import numpy as np
from scipy import linalg


def task_func(x):
    return math.sin(x / 5.) * math.exp(x / 10.) + 5. * math.exp(-x / 2.)


def first(x):
    return 3.43914511 + x * -0.18692825


def second(x):
    return 3.32512949 + x * -0.06531159 + x ** 2 * -0.00760104


def gen(a, x):
    sum = 0
    for i in xrange(0, len(a)):
        sum += a[i] * x ** i
    return sum


def points_to_coeffs(points):
    res = []
    for p in points:
        str = [1]
        for i in xrange(1, len(points)):
            str.append(p ** i)
        res.append(str)
    print res
    return res

print task_func(85)
print task_func(99)
print "That was fs"

# d = np.arange(0, 15, 0.1)
# plt.plot(d, [f(a) for a in d])
# plt.show()

print task_func(1), task_func(15), task_func(8)


def mch_first_grade():
    print "first grade"
    x = np.array([[1, 1], [1, 15]])
    b = np.array([task_func(1), task_func(15)])
    print "x =", x
    A = linalg.solve(x, b)
    print "A =", A
    d = np.arange(0, 15, 0.1)
    plt.plot(d, [gen(A, v) for v in d])
    plt.show()
    # res = [f(a) for a in x]
    # print "f(x) =", res
    # plt.plot(x, res)
    # plt.show()


def mch_second_grade():
    print "second grade"
    x = np.array([[1, 1, 1], [1, 8, 64], [1, 15, 15 ** 2]])
    b = np.array([task_func(1), task_func(8), task_func(15)])
    print "x =", x
    A = linalg.solve(x, b)
    print "A =", A
    d = np.arange(0, 15, 0.1)
    plt.plot(d, [gen(A, v) for v in d])
    plt.show()


def mch_third_grade():
    print "third grade"
    x = np.array(points_to_coeffs([1, 4, 10, 15]))
    b = np.array([task_func(1), task_func(4), task_func(10), task_func(15)])
    print "x =", x
    A = linalg.solve(x, b)
    print "A =", A
    d = np.arange(0, 15, 0.1)
    plt.plot(d, [gen(A, v) for v in d])
    plt.plot(d, [task_func(a) for a in d])
    plt.show()


def mch_any_grade(points, f):
    print "any grade"
    x = np.array(points_to_coeffs(points))
    b = np.array([f(a) for a in points])
    print "x =", x
    print "b =", b
    A = linalg.solve(x, b)
    print "A =", A
    d = np.arange(-5, 17, 0.1)
    plt.plot(d, [gen(A, v) for v in d])
    plt.plot(d, [f(a) for a in d])
    plt.show()


print
# mch_first_grade()
print
# mch_second_grade()
print
# mch_third_grade()

print
# mch_any_grade([1, 4, 7, 15, 20, 25, 37, 73, 85, 100])
mch_any_grade([-1, -4, 3, 6], task_func)
