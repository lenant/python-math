# coding=utf-8
from matplotlib.colors import ListedColormap
from sklearn import cross_validation, datasets, linear_model, metrics
import matplotlib.pyplot as plt

import numpy as np


# this works
def classify(linear_classifier, features):
    res = np.array([])
    for feature in features:
        counted = linear_classifier.intercept_ + linear_classifier.coef_[0][0] * feature[0] + linear_classifier.coef_[0][1] * feature[1]
        res = np.append(res, [1 if counted[0] > 0 else 0])
    return res


def y_for_line(linear_classifier, x_array):
    y_array = np.array([])
    for x in x_array:
        y = -(ridge_classifier.intercept_  + linear_classifier.coef_[0][0] * x) / linear_classifier.coef_[0][1]
        y_array = np.append(y_array, [y])
    return y_array


blobs = datasets.make_blobs(centers=2, cluster_std=5.5,  n_samples=1000, random_state=1)
# print blobs

train_data, test_data, train_labels, test_labels = cross_validation.train_test_split(blobs[0], blobs[1],
                                                                                     test_size=0.3,
                                                                                     random_state=1)

# создание объекта - классификатора
ridge_classifier = linear_model.RidgeClassifier(random_state=1)
#обучение классификатора
ridge_classifier.fit(train_data, train_labels)
#применение обученного классификатора
ridge_predictions = ridge_classifier.predict(test_data)

print test_labels
print ridge_predictions
my_classification = classify(ridge_classifier, test_data)
print my_classification
print "accuracy = ", metrics.accuracy_score(test_labels, ridge_predictions)

for i in xrange(0, len(my_classification)):
    if my_classification[i] != ridge_predictions[i]:
        print "NOT EQUAL"

print ridge_classifier.coef_

print ridge_classifier.intercept_

colors = ListedColormap(['red', 'blue'])

# plt.figure(plt.figsize(8, 8))
plt.scatter(map(lambda x: x[0], blobs[0]), map(lambda x: x[1], blobs[0]), c = blobs[1], cmap = colors)
x_array = range(-25, 15)
y_array = y_for_line(ridge_classifier, x_array)
plt.plot(x_array, y_array)
plt.show()