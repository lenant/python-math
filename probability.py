# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sts
import math


def norm(mu, sigma):
    # зададим нормально распределенную случайную величину
    norm_rv = sts.norm(loc=mu, scale=sigma)

    # сгенерируем 10 значений
    print norm_rv.rvs(size=10)

    x = np.linspace(0, 4, 100)
    cdf = norm_rv.cdf(x)  # функция может принимать и вектор (x)
    plt.plot(x, cdf)
    plt.ylabel('$F(x)$')
    plt.xlabel('$x$')
    plt.show()

    pdf = norm_rv.pdf(x)
    plt.plot(x, pdf)

    plt.ylabel('$f(x)$')
    plt.xlabel('$x$')
    plt.show()


def binom(n, p):
    binomial_rv = sts.binom(n, p)
    print binomial_rv.rvs(10)
    print binomial_rv.rvs(10)
    print binomial_rv.rvs(10)
    print binomial_rv.rvs(10)

    # функция распределения
    x = np.linspace(0, n, n + 1)
    cdf = binomial_rv.cdf(x)
    plt.step(x, cdf)

    plt.ylabel('$F(x)$')
    plt.xlabel('$x$')
    plt.show()

    # функция плотности
    x = np.linspace(0, n, n + 1)
    pmf = binomial_rv.pmf(x)
    plt.plot(x, pmf, 'o')

    plt.ylabel('$P(X=x)$')
    plt.xlabel('$x$')
    plt.show()


def binom_hist(n, p):
    binomial_rv = sts.poisson(12)
    means = []
    for i in xrange(0, 100000):
        sample = binomial_rv.rvs(30)
        # print sample
        mean = np.mean(sample)
        means.append(mean)

    # print "means =", means
    plt.hist(means)
    plt.show()


def poisson(m):
    poisson_rv = sts.poisson(m)
    poisson_rv.rvs(10)

    x = np.linspace(0, 30, 31)
    pmf = poisson_rv.pmf(x)
    print "%.2f" % pmf[5]
    plt.plot(x, pmf, "o")
    plt.show()


def truncnorm():
    a, b = 0.1, 2

    # print sts.truncnorm.ppf(0.01, a, b)
    # print sts.truncnorm.ppf(0.99, a, b)

    x = np.linspace(sts.truncnorm.ppf(0.01, a, b),
                    sts.truncnorm.ppf(0.99, a, b), 100)

    plt.plot(x, sts.truncnorm.pdf(x, a, b),
             'r-', lw=5, alpha=0.6, label='truncnorm pdf')

    values = sts.truncnorm.rvs(a, b, size=1000)

    plt.hist(values, normed=True, histtype='stepfilled', alpha=0.2)

    plt.show()


def expon_1():
    x = np.linspace(sts.expon.ppf(0.01),
                    sts.expon.ppf(0.99), 100)

    plt.plot(x, sts.expon.pdf(x),
             'r-', lw=5, alpha=0.6, label='expon pdf')

    values = sts.expon.rvs(size=1000)

    plt.hist(values, normed=True, histtype='stepfilled', alpha=0.2)
    plt.legend(loc='best', frameon=False)
    plt.show()


def expon_2():
    x = np.linspace(sts.expon.ppf(0.01),
                    sts.expon.ppf(0.99), 100)

    lambd = 0.8
    scale = 1./lambd
    means = []
    for n in (500,):
        for i in xrange(0, 1000):
            values = sts.expon.rvs(size=n, scale=scale)
            mean = np.mean(values)
            # print values, mean
            means.append(mean)

    # plt.plot(x, sts.expon.pdf(x),
    #          'r-', lw=5, alpha=0.6, label='expon pdf')

    plt.plot(x, sts.norm.pdf(x, scale, math.sqrt((scale ** 1)/1000.)),
             'g-', lw=2, alpha=0.3, label='norm pdf')

    plt.hist(means, normed=True, alpha=0.2, label='expon hist')
    plt.legend(loc='best', frameon=False)

    plt.ylabel('$f(x)$')
    plt.xlabel('$x$')

    plt.xlim([0,3])

    plt.show()


def main():
    # norm(2, 0.5)
    # poisson(3)
    # binom_hist(15, 0.01)
    # expon_1()
    # expon_2()
    poisson(3)

if __name__ == "__main__":
    main()
