import math
from matplotlib import pylab as plt
import numpy as np
from scipy import linalg


def task_func(x):
    return math.sin(x / 5.) * math.exp(x / 10.) + 5. * math.exp(-x / 2.)


def derivative(f, x, delta=0.00000000001):
    print "x + delta =", x + delta
    f1 = f(x + delta)
    f2 = f(x)
    sub = f1 - f2
    res = sub / delta
    print "f(x + delta) =", f1
    print "f(x) =", f2
    print "f(x + delta) - f(x) =", sub
    print "(f(x + delta) - f(x)) / delta =", res
    return res


def tangent(fx0, x0, x, coeff):
    return fx0 + coeff * (x - x0)


def draw_tangent(f, x0):
    fx0 = f(x0)
    coeff = derivative(f, x0)
    print "coeff =", coeff
    d = np.arange(0, 15, 0.1)
    tangent_points = [tangent(fx0, x0, a, coeff) for a in d]
    plt.plot(d, [f(a) for a in d])
    plt.plot(d, tangent_points)
    plt.show()
    

draw_tangent(task_func, 12)
