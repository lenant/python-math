# coding=utf-8

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


def write_answer_to_file(answer, filename):
    with open(filename, 'w') as f_out:
        f_out.write(str(round(answer, 3)))


def test(x):
    print x


def mserror(y, y_pred):
    return np.mean((y - y_pred) ** 2)


def normal_equation(X, y):
    dot = np.dot(X.T, X)
    inverse = np.linalg.inv(dot)
    return np.dot(np.dot(inverse, X.T), y)


def linear_prediction(X, w):
    return np.dot(X, w)


# def gradient_step(X, y, w, eta=0.01):
#     grad = 2  / X.shape[0] * np.sum(((y - np.dot(X, w)) ** 2) * X)


def stochastic_gradient_step(X, y, w, train_ind, eta=0.01):
    # print
    grad0 = grad(X, train_ind, w, y[train_ind], 0)
    grad1 = grad(X, train_ind, w, y[train_ind], 1)
    grad2 = grad(X, train_ind, w, y[train_ind], 2)
    grad3 = grad(X, train_ind, w, y[train_ind], 3)
    grad_ = eta * np.array([grad0, grad1, grad2, grad3])
    # print "grad_ = ", grad_
    # print "w = ", w
    res = w - grad_ / X.shape[0]# why do we divide???
    # print "res = ", res
    return res

    # from some guy from forum
    # row = X[train_ind]
    # l = X.shape[0]
    # # print "row ", row, "w ",w, "y[train_ind] ", y[train_ind]
    # grad_his = row.dot(w) - y[train_ind]
    # ans = w - 2 * eta / l * grad_his * X[train_ind]
    # return ans


def grad(X, train_ind, w, y, j):
    x = X[train_ind]
    # print "x = ", x, "x[j] =", x[j], "y = ", y
    grad_res = 2. * (np.dot(w, x) - y) * x[j]
    # print "grad_res = ", grad_res
    return grad_res

def stochastic_gradient_descent(X, y, w_init, eta=1e-2, max_iter=1e5,
                                    min_weight_dist=1e-8, seed=42, verbose=True):


    # Инициализируем расстояние между векторами весов на соседних
    # итерациях большим числом.
    weight_dist = np.inf
    # Инициализируем вектор весов
    w = w_init
    # Сюда будем записывать ошибки на каждой итерации
    errors = []
    # Счетчик итераций
    iter_num = 0
    # Будем порождать псевдослучайные числа
    # (номер объекта, который будет менять веса), а для воспроизводимости
    # этой последовательности псевдослучайных чисел используем seed.
    np.random.seed(seed)

    # Основной цикл
    while weight_dist > min_weight_dist and iter_num < max_iter:
        # порождаем псевдослучайный
        # индекс объекта обучающей выборки
        random_ind = np.random.randint(X.shape[0])

        # делаем шаг, пишем ошибку

        w_temp = w
        w = stochastic_gradient_step(X, y, w, random_ind, eta)
        y_pred = linear_prediction(X[random_ind], w)
        errors.append(mserror(y[random_ind], y_pred))
        weight_dist = np.sqrt((w - w_temp).dot(w - w_temp))

        iter_num = iter_num + 1

    print "iter_num = ", iter_num
    print "weight_dist = ", weight_dist

    return w, errors


def main():
    adver_data = pd.read_csv('advertising.csv')
    print adver_data.head(5)

    print

    X = np.array([adver_data["TV"], adver_data["Radio"], adver_data["Newspaper"]]).T
    print "X=", X[:5]
    y = np.array(adver_data["Sales"])
    print "y=", y[:5]

    print

    means = np.array([X[:, 0].mean(), X[:, 1].mean(), X[:, 2].mean()])
    stds = np.array([X[:, 0].std(), X[:, 1].std(), X[:, 2].std()])

    print "means = ", means
    print "stds = ", stds

    print

    X = X.T
    for i in range(0, 3):
        X[i] = (X[i] - means[i]) / stds[i]
    X = X.T
    print X[:5]

    print

    ones = np.ones((X.shape[0], 1))
    print ones[:5]

    X = np.hstack((X, ones))

    print X[:5]

    print "---- done hstack\n"

    median = np.median(y)
    print "median y = ", median
    mserror_value = mserror(y, median)
    print "mserror_value y = ", mserror_value

    write_answer_to_file(mserror_value, '1.txt')

    print "---- done mserror for median"

    anal_w = normal_equation(X, y)
    print "anal_w = ", anal_w

    answer2 = np.dot(anal_w, np.array([0, 0, 0, 1]))
    print(answer2)
    write_answer_to_file(answer2, '2.txt')

    print "---- done for zeroes"

    predicted = linear_prediction(X, anal_w)
    mserror_for_predicted = mserror(y, predicted)
    print "mserror_for_predicted = ", mserror_for_predicted
    write_answer_to_file(mserror_for_predicted, '3.txt')

    # results = in_loop(X, y)
    # print results
    # print "mean =", np.mean(results)
    # print "median    =", np.median(results)

    stoch_grad_desc_weights, stoch_errors_by_iter = stochastic_gradient_descent(X, y, np.array([0, 0, 0, 0]))

    print "stoch_grad_desc_weights, stoch_errors_by_iter = ", stoch_grad_desc_weights#, stoch_errors_by_iter

    plt.plot(range(50), stoch_errors_by_iter[:50])
    plt.xlabel('Iteration number')
    plt.ylabel('MSE')
    plt.show()

    plt.plot(range(2500), stoch_errors_by_iter[-2500:])
    plt.xlabel('Iteration number')
    plt.ylabel('MSE')
    plt.show()

    stoch_grad_desc_weights = np.reshape(stoch_grad_desc_weights, 4)
    print "reshaped stoch_grad_desc_weights = ", stoch_grad_desc_weights

    stoch_used = linear_prediction(X, stoch_grad_desc_weights)

    answer4 = mserror(y, stoch_used)
    print(answer4)
    write_answer_to_file(answer4, '4.txt')


def in_loop(X, y):
    results = np.array([])
    for i in range(20):
        stoch_grad_desc_weights, stoch_errors_by_iter = stochastic_gradient_descent(X, y, np.array([0, 0, 0, 0]))
        print stoch_grad_desc_weights
        stoch_grad_desc_weights = np.reshape(stoch_grad_desc_weights, 4)
        print "reshaped stoch_grad_desc_weights = ", stoch_grad_desc_weights
        stoch_used = linear_prediction(X, stoch_grad_desc_weights)
        answer4 = mserror(y, stoch_used)
        print answer4
        results = np.append(results, answer4)
        print
    return results


if __name__ == "__main__":
    main()
